class RenameFieldLabel < ActiveRecord::Migration[6.0]
  def up
    rename_column :exams, :label, :level
  end
end
