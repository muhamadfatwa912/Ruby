# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.create(name: 'islam', page: 50, price: 190000, description: 'SUbhanallah')
Student.create(name: 'kristen', page: 40, price: 9900000, description: 'haleluya')
Student.create(name: 'budha', page: 30, price: 500000, description: 'baik')
Student.create(name: 'kong hu cu', page: 90, price: 88000, description: 'keren')
Student.create(name: 'hindu', page: 10, price: 90000, description: 'bagus')